{-# LANGUAGE OverloadedStrings #-}

module Client where

import Control.Monad
import Control.Concurrent
import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString (send, recv)

client :: IO ()
client = do
    addrInfo <- getAddrInfo Nothing (Just "localhost") (Just "3000")
    let serverAddr = head addrInfo
    replicateM_ 1000 $ forkIO $ do
        sock <- socket (addrFamily serverAddr) Stream defaultProtocol
        connect sock (addrAddress serverAddr)
        sendMsg sock

sendMsg :: Socket -> IO ()
sendMsg sock = forever $ do
    _ <- send sock "Hello"
    _ <- recv sock 16
    threadDelay 10000
