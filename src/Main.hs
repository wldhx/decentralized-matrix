{-# LANGUAGE OverloadedStrings #-}

module Main where

import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString
import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad
import qualified Data.ByteString as B


type TxList = TVar [B.ByteString]


main :: IO ()
main = do
    txlist <- newTVarIO [B.empty]

    sock <- mkSock
    listen sock 5
    forever $ acceptLoop sock txlist

mkSock :: IO Socket
mkSock = do
    sock <- socket AF_INET6 Stream defaultProtocol
    setSocketOption sock ReuseAddr 1
    setSocketOption sock ReusePort 1
    setSocketOption sock IPv6Only 1
    bind sock $ SockAddrInet6 3000 0 iN6ADDR_ANY 0
    return sock

acceptLoop :: Socket -> TxList -> IO ()
acceptLoop sock txlist = do
    (csock, addr) <- accept sock
    void $ forkFinally (doConnection csock addr txlist) (\_ -> close csock)

doConnection :: Socket -> SockAddr -> TxList -> IO ()
doConnection sock addr txlist = forever $ do
    _ <- send sock =<< head `liftM` (atomically . readTVar) txlist
    recv sock 16 >>= (atomically . modifyTVar txlist) . (:)
    threadDelay 10000
