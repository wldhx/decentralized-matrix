{ nixpkgs ? (import ((import <nixpkgs> {}).fetchFromGitHub (builtins.fromJSON (builtins.readFile ./nixpkgs.json)))) {} }:
(import ./default.nix { inherit nixpkgs; }).env
