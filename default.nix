{ nixpkgs ? (import ((import <nixpkgs> {}).fetchFromGitHub (builtins.fromJSON (builtins.readFile ./nixpkgs.json)))) {} }:
nixpkgs.pkgs.haskellPackages.callPackage ./decentralized-matrix.nix { }
