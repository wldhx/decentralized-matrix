{ mkDerivation, base, network, stdenv, stm, cabal-install }:
mkDerivation {
  pname = "decentralized-matrix";
  version = "0.0.0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base network stm ];
  buildDepends = [ cabal-install ];
  homepage = "https://gitlab.com/wldhx/decentralized-matrix";
  description = "A P2P eventually consistent DAG with decentralized identity";
  license = stdenv.lib.licenses.agpl3;
}
